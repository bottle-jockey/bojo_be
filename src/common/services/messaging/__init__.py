import os
from twilio.rest import Client
from .templates import get_templates

"""
    method: send_message
    params: to_phone_number, body
    descripition:  sends a message with contents as body to the to_phone_number
"""
def send_message(to_phone_number, body):
    # Find your Account SID and Auth Token at twilio.com/console
    # and set the environment variables. See http://twil.io/secure
    ACCOUNT_SID = os.environ['TWILIO_ACCOUNT_SID']
    AUTH_TOKEN = os.environ['TWILIO_AUTH_TOKEN']
    FROM_PHONE_NUMBER = os.environ['TWILIO_FROM_PHONE_NUMBER']
    
    client = Client(ACCOUNT_SID, AUTH_TOKEN)
    response = client.messages.create(body=body, from_=FROM_PHONE_NUMBER, to=to_phone_number)
    return response

"""
    method: send_OTP
    params: to_phone_number, otp
    descripition:  sends an OTP message to the to_phone_number
"""
def send_OTP(to_phone_number, otp):
    if os.getenv('DJANGO_DEBUG'):
        return None
    body = get_templates('OTP').format(OTP=otp)
    response = send_message(to_phone_number, body)
    return response
