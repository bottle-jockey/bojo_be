MESSAGE_TEMPLATES = {
    'OTP': "Your otp is {OTP}, and it will expire in 5 minutes"
}

def get_templates(type):
    return MESSAGE_TEMPLATES[type]