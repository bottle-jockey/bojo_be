from django.db import models

class Meta(models.Model):
    class Meta:
        verbose_name_plural = "Meta"
        
    key = models.CharField(max_length=64, null=False, blank=False)
    value = models.CharField(max_length=64, null=False, blank=False)
    