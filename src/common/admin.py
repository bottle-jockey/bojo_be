from django.contrib import admin
from django.contrib.auth.models import Group
from django_summernote.models import Attachment
from rest_framework.authtoken.models import TokenProxy
from django_celery_beat.models import SolarSchedule, IntervalSchedule, PeriodicTask, ClockedSchedule, CrontabSchedule
from django_rest_passwordreset.models import ResetPasswordToken
from social_django.models import Association, Nonce
from .models import Meta


admin.site.unregister(Group)
admin.site.unregister(SolarSchedule)
admin.site.unregister(IntervalSchedule)
admin.site.unregister(PeriodicTask)
admin.site.unregister(ClockedSchedule)
admin.site.unregister(CrontabSchedule)
# admin.site.unregister(ResetPasswordToken)
admin.site.unregister(Attachment)
admin.site.unregister(Association)
admin.site.unregister(Nonce)
admin.site.unregister(TokenProxy)
admin.site.register(Meta)
