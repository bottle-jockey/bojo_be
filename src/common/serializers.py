from django.conf import settings
from rest_framework.serializers import ImageField as ApiImageField
from easy_thumbnails.files import get_thumbnailer
from rest_framework import status
from rest_framework.response import Response
from datetime import datetime

THUMBNAIL_ALIASES = getattr(settings, 'THUMBNAIL_ALIASES', {})

def get_url(request, instance, alias_obj, alias=None):
    if alias is not None:
        return request.build_absolute_uri(get_thumbnailer(instance).get_thumbnail(alias_obj[alias]).url)
    elif alias is None:
        return request.build_absolute_uri(instance.url)
    else:
        raise TypeError('Unsupported field type')

def image_sizes(request, instance, alias_obj):
    i_sizes = list(alias_obj.keys())
    return {
        'original': get_url(request, instance, alias_obj),
        **{k: get_url(request, instance, alias_obj, k) for k in i_sizes}
    }

def success_response(data, code=1000, status=status.HTTP_200_OK):
    data = {
        'success': True,
        'status': status,
        'code': code,
        'data': data,
        'timestamp': datetime.now()
    }
    return Response(data, status=status)

def error_response(data, code=5000, status=status.HTTP_500_INTERNAL_SERVER_ERROR):
    data = {
        'success': False,
        'code': code,
        'status': status,
        'error': data,
        'timestamp': datetime.now()
    }
    return Response(data, status=status)


class ThumbnailerJSONSerializer(ApiImageField):

    def __init__(self, alias_target, **kwargs):
        self.alias_target = THUMBNAIL_ALIASES.get(alias_target)
        super(ThumbnailerJSONSerializer, self).__init__(**kwargs)

    def to_representation(self, instance):
        if instance:
            return image_sizes(self.context['request'], instance, self.alias_target)
        return None
