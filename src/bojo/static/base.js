(function ($) {
    $(function () {
        console.log("Hi")
        var selectField = $('#id_type'),
            with_image = $('.with_image'),
            with_video = $('.with_video');

        function toggleVerified(value) {
            console.log(value)
            if (value === 'text_with_cover_image') {
                with_image.show();
                with_video.hide();
            } else if (value === 'text_with_video') {
                with_video.show();
                with_image.hide();
                console.log(with_image)
                console.log()
            } else {
                with_image.hide();
                with_video.hide();
            }
        }

        // show/hide on load based on pervious value of selectField
        toggleVerified(selectField.val());

        // show/hide on change
        selectField.change(function () {
            console.log("Calling")
            toggleVerified($(this).val());
        });
    });
})(django.jQuery);