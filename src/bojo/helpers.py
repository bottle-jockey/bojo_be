from .models import EventAttendee

"""
    method: is_user_a_co_host
    params: event, user
    descripition: checks if user is the event's co host
"""
def is_user_a_co_host(event, user):
    try:
        event_attendee = EventAttendee.objects.get(event=event, user=user, is_co_host=True)
    except Exception as e:
        return False
    return True