from rest_framework import serializers
from ..models import Event
from .event_attendee import EventAttendeeHostSerializer
from .invitation import InvitationSerializer

class EventSerializer(serializers.ModelSerializer):
    class Meta:
        model = Event
        fields = (
            'id',
            'name',
            'event_type',
            'expected_attendees_count',
            'event_date',
            'event_time',
            'is_host_attending',
            'is_attendees_bottles_required',
            'expected_bottles_count',
            'details',
            'contact_email',
            'contact_phone_number',
            'address',
            'coordinates',
            'status',
            'slug',
        )

class EventHostSerializer(serializers.ModelSerializer):
    in_app_guests = EventAttendeeHostSerializer(many=True, read_only=True)
    new_guests = InvitationSerializer(many=True, read_only=True)

    class Meta:
        model = Event
        fields = (
            'id',
            'name',
            'event_type',
            'expected_attendees_count',
            'event_date',
            'event_time',
            'is_host_attending',
            'is_attendees_bottles_required',
            'expected_bottles_count',
            'details',
            'contact_email',
            'contact_phone_number',
            'address',
            'coordinates',
            'status',
            'slug',
            'in_app_guests',
            'new_guests'
        )

class EventGuestSerializer(serializers.ModelSerializer):
    host_email_id = serializers.CharField(source='event_host.email')

    class Meta:
        model = Event
        fields = (
            'id',
            'name',
            'event_type',
            'event_date',
            'event_time',
            'is_attendees_bottles_required',
            'details',
            'contact_email',
            'contact_phone_number',
            'host_email_id',
            'address',
            'coordinates',
            'status',
            'slug',
        )