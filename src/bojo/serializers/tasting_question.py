from django.conf import settings
from rest_framework import serializers
from ..models import TastingQuestion

class HostTastingQuestionSerializer(serializers.ModelSerializer):
    class Meta:
        model = TastingQuestion
        fields = [
            "id",
            'supported_plans',
            "host_label",
            "host_description",
            "host_section",
            "is_default",
        ]