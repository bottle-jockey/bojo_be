from django.db.models import Q
from django.conf import settings
from rest_framework import serializers

from ..models import EventAttendee
from src.users.serializers import UserSerializer


class FilteredListSerializer(serializers.ListSerializer):
    """Serializer to filter the active system, which is a boolen field in 
       System Model. The value argument to to_representation() method is 
      the model instance"""
    
    def to_representation(self, data):
        data = data.exclude(status=settings.EVENT_ATTENDEE_STATUS['REMOVED'])
        return super(FilteredListSerializer, self).to_representation(data)

class EventAttendeeHostSerializer(serializers.ModelSerializer):
    first_name = serializers.CharField(source='user.first_name')
    last_name = serializers.CharField(source='user.last_name')
    profile_picture = serializers.CharField(source='user.profile_picture')
    attendee_id = serializers.IntegerField(source='id')

    class Meta:
        model = EventAttendee
        list_serializer_class = FilteredListSerializer
        fields = [
            'attendee_id',
            'email',
            'status',
            'first_name',
            'last_name',
            'profile_picture',
            'is_bringing_bottle',
            'is_co_host',
            'is_taster'
        ]

class EventAttendeeGuestSerializer(serializers.ModelSerializer):
    event_id = serializers.IntegerField(source='event.id')
    name = serializers.CharField(source='event.name')
    event_type = serializers.CharField(source='event.event_type') 
    event_date = serializers.CharField(source='event.event_date') 
    event_time = serializers.CharField(source='event.event_time')
    expected_bottles_count = serializers.IntegerField(source='event.expected_bottles_count')
    is_attendees_bottles_required = serializers.BooleanField(source='event.is_attendees_bottles_required') 
    details = serializers.CharField(source='event.details') 
    contact_email = serializers.CharField(source='event.contact_email') 
    contact_phone_number = serializers.CharField(source='event.contact_phone_number')
    address = serializers.JSONField(source='event.address')
    coordinates = serializers.ListField(child=serializers.FloatField(), source='event.coordinates')
    event_status = serializers.CharField(source='event.status')
    attendee_id = serializers.IntegerField(source='id')
    
    class Meta:
        model = EventAttendee
        fields = [
            'event_id',
            'attendee_id',
            'name',
            'event_type',
            'event_date',
            'event_time',
            'expected_bottles_count',
            'is_attendees_bottles_required',
            'details',
            'contact_email',
            'contact_phone_number',
            'address',
            'coordinates',
            'event_status',
            'is_taster',
            'is_co_host',
            'is_bringing_bottle',
            'status'
        ]
