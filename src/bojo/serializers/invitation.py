from django.conf import settings
from rest_framework import serializers
from ..models import Invitation

class FilteredListSerializer(serializers.ListSerializer):
    """Serializer to filter the active system, which is a boolen field in 
       System Model. The value argument to to_representation() method is 
      the model instance"""
    def to_representation(self, data):
        data = data.exclude(status=settings.INVITATION_STATUS['ACCEPTED'])
        return super(FilteredListSerializer, self).to_representation(data)

class InvitationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Invitation
        list_serializer_class = FilteredListSerializer
        fields = (
            'email',
            'status',
            'role'
        )