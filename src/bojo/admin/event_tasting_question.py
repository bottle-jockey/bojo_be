from django.contrib import admin
from ..models import EventTastingQuestion

class EventTastingQuestionAdmin(admin.ModelAdmin):
    list_display = ('event', 'tasting_question')
