from django.contrib import admin
from ..models import Event, Invitation, EventAttendee, Article, TastingQuestion, EventTastingQuestion
from .article import ArticleAdmin
from .event_attendee import EventAttendeeAdmin
from .event import EventAdmin
from .invitation import InvitationAdmin
from .event_tasting_question import EventTastingQuestionAdmin

admin.site.register(Invitation, InvitationAdmin)
admin.site.register(Article, ArticleAdmin)
admin.site.register(EventAttendee, EventAttendeeAdmin)
admin.site.register(Event, EventAdmin)
admin.site.register(TastingQuestion)
admin.site.register(EventTastingQuestion, EventTastingQuestionAdmin)