from django.contrib import admin
from ..models import EventAttendee

class EventAttendeeAdmin(admin.ModelAdmin):
    list_display = ('id', 'event', 'user', 'email', 'status', 'is_taster', 'is_co_host', 'updated_at')
