from django.contrib import admin

class InvitationAdmin(admin.ModelAdmin):
    list_display = ('email', 'token', 'status', 'event', 'expires_at', 'role')