from django.contrib import admin
from ..models import Article
from django.utils.translation import gettext_lazy as _

class ArticleAdmin(admin.ModelAdmin):

    fieldsets = (
        (None, {
            'fields': ('type', 'title', 'description', 'url',),
            'classes': ('predefined',)
        }),(None, {
            'fields': (('cover_image',),),
            'classes': ('with_image',)
        }),(None, {
            'fields': (('video_embed',),),
            'classes': ('with_video',)
        })
    )

    class Media:
        js = (
            '//ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js',
            'base.js',
        )
