from django.contrib import admin

class EventAdmin(admin.ModelAdmin):
    list_display = (
        'id', 
        'name',
        'event_type',
        'expected_attendees_count',
        'event_date',
        'event_time',
        'is_host_attending',
        'is_attendees_bottles_required',
        'expected_bottles_count',
        'details',
        'contact_email',
        'contact_phone_number',
        'address',
        'coordinates',
        'slug'
    )