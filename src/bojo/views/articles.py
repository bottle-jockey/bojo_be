from django.shortcuts import render
from rest_framework import viewsets, mixins
from ..serializers import ArticleSerializer
from ..models import Article
from src.common.serializers import success_response
# Create your views here.

class ArticleViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    serializer_class = ArticleSerializer
    queryset = Article.objects.all()

    def list(self, request, *args, **kwargs):
        response = super().list(request, args, kwargs)
        return success_response(response.data)