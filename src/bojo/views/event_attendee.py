from rest_framework.decorators import api_view, permission_classes
from ..models import EventAttendee
from ..helpers import is_user_a_co_host
from rest_framework.permissions import IsAuthenticated
from rest_framework.status import HTTP_400_BAD_REQUEST, HTTP_404_NOT_FOUND, HTTP_401_UNAUTHORIZED
from src.common.serializers import success_response, error_response

"""
    method: update
    params: event parameters to be updated
    descripition:  updates an event attendee
"""
@api_view(['PATCH'])
@permission_classes([IsAuthenticated])
def patch_event_attendee(request, attendee_id):
    data = request.data
    is_bringing_bottle = None
    is_taster = None
    
    # #get is_bringing_bottle value or is_taster
    if 'is_bringing_bottle' in data:
        is_bringing_bottle = data['is_bringing_bottle']
    elif 'is_taster' in data:
        is_taster = data['is_taster']

    if not (is_bringing_bottle != None or is_taster != None):
        return error_response (
            { 'message': 'is_bringing_bottle or is_taster is required'},
            status=HTTP_400_BAD_REQUEST
        )

    #getting event_attendee using attendee_id
    try:
        event_attendee = EventAttendee.objects.get(id=attendee_id)
    except EventAttendee.DoesNotExist:
        return error_response (
            { 'message': 'event attendee not found'},
            status=HTTP_404_NOT_FOUND
        )
    
    event = event_attendee.event

    if not is_bringing_bottle == None:
        #checking if attendees should bring their bottles to this event
        if not event.is_attendees_bottles_required:
            return error_response (
                { 'message': 'Attendees need not bring their bottles for this event'},
                status=HTTP_400_BAD_REQUEST
            )

        #updating the event_attendee record
        event_attendee.is_bringing_bottle = is_bringing_bottle
    elif not is_taster == None:
        #this request if for cohost to update their is_taster status
        #check if the user is a co_host in this event
        if not is_user_a_co_host(event, request.user):
            return error_response({ 'message': "not authorized"}, status=HTTP_401_UNAUTHORIZED)
        
        #if the user is a co_host
        event_attendee.is_taster = is_taster
    
    event_attendee.save()

    return success_response({ 'message': "event attendee updated"})
