from django.conf import settings
from django.shortcuts import render
from rest_framework.parsers import JSONParser
from rest_framework.decorators import api_view, permission_classes, parser_classes
from rest_framework import viewsets, mixins
from ..serializers import *
from ..models import Event, Invitation, EventAttendee
from rest_framework.permissions import IsAuthenticated
from src.common.serializers import success_response, error_response
from datetime import datetime
from django.utils.text import slugify
from schema import Schema, And
from ..helpers import is_user_a_co_host
from rest_framework.status import HTTP_400_BAD_REQUEST, HTTP_404_NOT_FOUND, HTTP_500_INTERNAL_SERVER_ERROR, HTTP_401_UNAUTHORIZED

ADDRESS_SCHEMA = {
    'name': And(str, lambda s: len(s) > 0)
}

class EventViewSet(
    mixins.ListModelMixin, 
    mixins.RetrieveModelMixin, 
    mixins.CreateModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet
):
    permission_classes = [IsAuthenticated]
    serializer_class = EventSerializer
    lookup_field = 'slug'

    def get_queryset(self):
        """
        This view should return a list of all the events
        for the currently authenticated user.
        """
        user = self.request.user
        return Event.objects.filter(event_host=user)

    """
        method: is_event_date_valid
        params: event_date
        descripition: validates event date
    """
    def is_event_date_valid(self, event_date):
        event_date_obj = datetime.strptime(event_date, '%Y-%m-%d').date()
        if event_date_obj <= datetime.today().date():
            return None
        return event_date_obj
    
    """
        method: is_attendees_count_valid
        params: user, count
        descripition: checks if the attendees count is valid
    """
    def is_attendees_count_valid(self, user, count):
        return count <= settings.ATTENDEES_LIMIT_FOR_NON_PREMIUM or user.is_premium_user()
    
    """
        method: is_bottles_count_valid
        params: count
        descripition: checks if the bottles count is valid
    """
    def is_bottles_count_valid(self, count):
        return count >= settings.MINIMUM_BOTTLES_COUNT and count <= settings.MAXIMUM_BOTTLES_COUNT

    """
        method: is_address_valid
        params: address
        descripition: checks if the address object is valid
    """
    def is_address_valid(self, address):
        schema = Schema(ADDRESS_SCHEMA)
        try:
            schema.validate(address)
        except:
            return False
        return True

    def list(self, request, *args, **kwargs):
        response = super().list(request, args, kwargs)
        return success_response(response.data)
    
    """
        method: retrieve
        params: event_slug
        descripition:  returns the event details based on the type of user
    """
    def retrieve(self, request, *args, **kwargs):
        slug = kwargs['slug']
        try:
            event = Event.objects.get(slug=slug)
            if event.event_host == request.user or is_user_a_co_host(event, request.user):
                serializer = EventHostSerializer(event)
                data = {
                    'event': serializer.data
                }
                data['event']['share_link'] = "{}/api/v1/bojo/events/{}".format(settings.SITE_URL, slug)
            else:
                #getting the event attendee
                eventAttendee = EventAttendee.objects.filter(event=event, user=request.user)
                if eventAttendee:
                    """
                        if a record is present, then that user is an invited user
                        hence passing data from EventAttendeeGuestSerializer
                    """
                    serializer = EventAttendeeGuestSerializer(eventAttendee[0])
                else:
                    """
                        if a record is not present, this request is from a new
                        user, hence fetching event details
                    """
                    serializer = EventGuestSerializer(event)

                data = {
                    'event': serializer.data
                }

        except Event.DoesNotExist:
            return error_response({ 'message': "event not found"}, status=HTTP_400_BAD_REQUEST)
        except Exception as e:
            return error_response({ 'message': "Something went wrong"}, status=HTTP_500_INTERNAL_SERVER_ERROR)
        return success_response(data)
    
    """
        method: create
        params: name, expected_attendees_count, is_host_attending, event_type, details
        is_attendees_bottles_required, expected_bottles_count, event_date, event_time,
        descripition:  creates a new event
    """
    def create(self, request, *args, **kwargs):
        data = request.data
        event_time_obj = format(datetime.strptime(data['event_time'], '%I:%M%p'),"%H:%M:%S")
        contact_email = None
        contact_phone_number = None
        event_address = None
        event_coordinates = None
        
        #validating name
        if not data['name']:
            return error_response (
                { 'message': 'invalid event name'},
                status=HTTP_400_BAD_REQUEST
            )

        #validationg number of attendes
        if(data['expected_attendees_count'] > settings.MAXIMUM_ATTENDEES_COUNT):
            return error_response (
                { 'message': "cannot add more than {COUNT} attendees".format(COUNT=settings.MAXIMUM_ATTENDEES_COUNT)},
                status=HTTP_400_BAD_REQUEST
            )

        if not self.is_attendees_count_valid(request.user, data['expected_attendees_count']):
            return error_response (
                { 
                    'message': "non premium users can add a maximum of {COUNT} attendees".format(
                        COUNT=settings.ATTENDEES_LIMIT_FOR_NON_PREMIUM
                    )
                }, 
                status=HTTP_400_BAD_REQUEST
            )
        #validating event date
        event_date_obj = self.is_event_date_valid(data['event_date'])
        
        if not event_date_obj:
            return error_response(
                { 'message': "user can create an event only for a future date"}, 
                status=HTTP_400_BAD_REQUEST
            )
        
        if data['include_contact_info']:
            if 'contact_email' not in data:
                return error_response(
                    { 'message': "contact email is required"}, 
                    status=HTTP_400_BAD_REQUEST
                )
            elif 'contact_phone_number' not in data:
                return error_response(
                    { 'message': "contact phone number is required"}, 
                    status=HTTP_400_BAD_REQUEST
                )
                
            contact_email = data['contact_email']
            contact_phone_number = data['contact_phone_number']

        # validating bottles count
        if not self.is_bottles_count_valid(data['expected_bottles_count']):
            return error_response(
                    { 'message': "the number of bottles must be between {MIN} - {MAX}".format(
                        MIN=settings.MINIMUM_BOTTLES_COUNT,
                        MAX=settings.MAXIMUM_BOTTLES_COUNT
                    )}, 
                    status=HTTP_400_BAD_REQUEST
                )
        
        #checking for address
        if 'address' in data:
            if not self.is_address_valid(data['address']):
                return error_response(
                { 'message': "invalid address"}, 
                status=HTTP_400_BAD_REQUEST
            )
            event_address = data['address']
        
        #checking for checking coordinates
        if 'coordinates' in data:
            if len(data['coordinates']) != 2:
                return error_response(
                { 'message': "invalid coordinates"}, 
                status=HTTP_400_BAD_REQUEST
            )
            event_coordinates = data['coordinates']

        serialized_event = EventSerializer(data=data)
        if(serialized_event.is_valid):
            event = Event(
                name = data['name'],
                expected_attendees_count = data['expected_attendees_count'],
                is_host_attending = data['is_host_attending'],
                event_type = data['event_type'],
                is_attendees_bottles_required=data['is_attendees_bottles_required'],
                expected_bottles_count = data['expected_bottles_count'],
                event_date = event_date_obj,
                event_time = event_time_obj,
                details = data['details'],
                event_host = request.user,
                contact_email = contact_email,
                contact_phone_number = contact_phone_number,
                address = event_address,
                coordinates = event_coordinates,
                status = settings.EVENT_STATUS_CHOICES['SCHEDULED']
            )

            event.save()

            #adding unique slug
            event.slug = "{name}-{id}".format(
                name=slugify(event.name), 
                id=event.id
            )

            event.save()

            #adding host to event_attendee
            event_attendee = EventAttendee(
                event=event,
                user=request.user,
                email=request.user.email,
                status=settings.EVENT_ATTENDEE_STATUS['ACCEPTED'],
                is_taster=data['is_host_attending']
            )

            event_attendee.save()

            serializer = EventSerializer(event)
        
        data = {
            'event': serializer.data
        }
        return success_response(data)
    
    """
        method: update
        params: event parameters to be updated
        descripition:  updates an event
    """
    def update(self, request, *args, **kwargs):
        data = request.data
        slug = kwargs['slug']

        try:
            event = Event.objects.get(slug=slug)
        except Event.DoesNotExist:
            return HttpResponse(status=HTTP_404_NOT_FOUND)
        
        for field in data:
            #checking for null values
            if field in (
                'expected_attendees_count',
                'event_type',
                'expected_bottles_count',
                'name',
                'details',
                'event_time',
                'event_date',
            ):
                if not data[field]:
                    return error_response (
                        { 'message': "{field} is invalid".format(field=field)}, 
                        status=HTTP_400_BAD_REQUEST
                    )

            #validating event_date
            if field == 'event_date':
                event_date_obj = self.is_event_date_valid(data[field])
                if not event_date_obj:
                    return error_response(
                        { 'message': "user can create an event only for a future date"}, 
                        status=HTTP_400_BAD_REQUEST
                    )
                setattr(event, field, event_date_obj)
                continue
            
            elif field == 'name':
                if not data[field]:
                    return error_response (
                        { 'message': 'invalid event name'},
                        status=HTTP_400_BAD_REQUEST
                    )
            
            #handling event time
            elif field == 'event_time':
                event_time = format(datetime.strptime(data['event_time'], '%I:%M%p'),"%H:%M:%S")
                setattr(event, field, event_time)
                continue
                    
            #validating expected attendees count
            elif field == 'expected_attendees_count':
                if(data[field] > settings.MAXIMUM_ATTENDEES_COUNT):
                    return error_response (
                        { 'message': "cannot add more than {COUNT} attendees".format(COUNT=settings.MAXIMUM_ATTENDEES_COUNT)}, 
                        status=HTTP_400_BAD_REQUEST
                    )
                if not self.is_attendees_count_valid(request.user, data[field]):
                    return error_response (
                        { 
                            'message': "non premium users can add a maximum of {COUNT} attendees".format(
                                COUNT=settings.ATTENDEES_LIMIT_FOR_NON_PREMIUM
                            )
                        },
                        status=HTTP_400_BAD_REQUEST
                    )
            
            # validating bottles count
            elif field == 'expected_bottles_count':
                if not self.is_bottles_count_valid(data['expected_bottles_count']):
                    return error_response(
                            { 'message': "the number of bottles must be between {MIN} - {MAX}".format(
                                MIN=settings.MINIMUM_BOTTLES_COUNT,
                                MAX=settings.MAXIMUM_BOTTLES_COUNT
                            )}, 
                            status=HTTP_400_BAD_REQUEST
                        )

            setattr(event, field, data[field])

        try:
            event.save()
        except Exception as e:
            return error_response({'message': e.args}, status=HTTP_400_BAD_REQUEST)
        
        serializer = EventSerializer(event)

        data = {
            'event': serializer.data
        }
        return success_response(data)
    
    """
        method: destroy
        params: event slug to be deleted
        descripition:  deletes an event
    """
    def destroy(self, request, *args, **kwargs):
        try:
            response = super().destroy(request, args, kwargs)
        except Event.DoesNotExist:
            return error_response({ 'message': "event not found"}, status=HTTP_400_BAD_REQUEST)
        return success_response({ 'message': 'event deleted successfully'})

"""
    method: designate_co_host
    params: request, attendee_id
            guest_email - email of the user to be made co host
    descripition:  Makes the corresponding guest as the co-host of the event
"""
@api_view(['POST'])
@permission_classes([IsAuthenticated])
@parser_classes([JSONParser,])
def designate_co_host(request, event_id, attendee_id):
    data = request.data
    #checking for event_id
    if not event_id:
        return error_response({ 'message': "invalid event id"}, status=HTTP_400_BAD_REQUEST)
    
    #checking for attendee_id
    if not attendee_id:
        return error_response({ 'message': "invalid guest id"}, status=HTTP_400_BAD_REQUEST)

    #getting the event
    try:
        event = Event.objects.get(id=event_id)
    except Event.DoesNotExist:
        return error_response({ 'message': "event not found"}, status=HTTP_400_BAD_REQUEST)

    #checking if the request is made by the event host
    if not event.event_host == request.user:
        return error_response({ 'message': "not authorized"}, status=HTTP_401_UNAUTHORIZED)

    #getting the attendee
    try:
        event_attendee = EventAttendee.objects.get(id=attendee_id)
    except EventAttendee.DoesNotExist:
        return error_response({ 'message': "Guest not found"}, status=HTTP_400_BAD_REQUEST)
    
    #checking the attendee has accepted the invite to make them the co_host
    if not event_attendee.status == settings.INVITATION_STATUS['ACCEPTED']:
        return error_response({ 'message': "Guest has not yet accepted the invite"}, status=HTTP_400_BAD_REQUEST)
    
    #checking if the attende is already a co_host
    if event_attendee.is_co_host:
        return error_response({ 'message': "Guest is already a co host"}, status=HTTP_400_BAD_REQUEST)
    
    #making the attendee the co_host
    event_attendee.is_co_host = True
    event_attendee.save()

    return success_response({ 'message': "Guest is made a co-host"})

"""
    method: remove_attendee_from_event
    params: request, event_id, attendee_id
            event_id - event id
            attendee_id - email of the user to removed from the event
    descripition:  removes the corresponding user from the event
"""
@api_view(['DELETE'])
@permission_classes([IsAuthenticated])
@parser_classes([JSONParser,])
def remove_attendee_from_event(request, event_id, attendee_id):
    data = request.data

     #checking for event_id
    if not event_id:
        return error_response({ 'message': "invalid event id"}, status=HTTP_400_BAD_REQUEST)
    
    #checking for attendee_id
    if not attendee_id:
        return error_response({ 'message': "invalid attendee id"}, status=HTTP_400_BAD_REQUEST)

    #getting the event
    try:
        event = Event.objects.get(id=event_id)
    except Event.DoesNotExist:
        return error_response({ 'message': "event not found"}, status=HTTP_400_BAD_REQUEST)

    #checking if the request is made by the event host
    if not (event.event_host == request.user or is_user_a_co_host(event, request.user)):
        return error_response({ 'message': "not authorized"}, status=HTTP_401_UNAUTHORIZED)
    
    try:
        event_attendee = EventAttendee.objects.get(id=attendee_id)
        #checking if the event_attendee is the host
        if event_attendee.user == event.event_host or event_attendee.user == request.user:
            """
                This block will be executed if the request user is part of the event
                (i.e) host or co_host
            """
            #if host or cohost, then we are making the attendee a non-taster
            event_attendee.is_taster = False
            event_attendee.save()

            #if host, update the is_host_attending flag
            if event_attendee.user == event.event_host:
                event.is_host_attending = False
                event.save()
                return success_response({ 'message': "Host is not a taster at this event anymore"})

            return success_response({ 'message': "You are not a taster at this event anymore"})
        else:
            #if not host,  make the status of the attendee as removed
            event_attendee.status = settings.EVENT_ATTENDEE_STATUS['REMOVED']
            event_attendee.is_co_host = False
            event_attendee.is_taster = False
            event_attendee.save()
    except EventAttendee.DoesNotExist:
        return error_response({ 'message': "Guest not found"}, status=HTTP_400_BAD_REQUEST)

    return success_response({ 'message': "Guest is removed from this event"})
