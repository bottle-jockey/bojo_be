from rest_framework.permissions import IsAuthenticated
from src.common.serializers import success_response
from rest_framework.decorators import api_view, permission_classes
from ..models import Event, Article, EventAttendee
from ..serializers import ArticleSerializer, EventSerializer, EventGuestSerializer
from datetime import datetime


HOME_VIEW_LIMIT = 3

"""
    method: home_view
    descripition:  Responds with the latest articles and upcoming event for the
    logged in user
"""
@api_view(['GET'])
@permission_classes([IsAuthenticated])
def home_view(request):

    today = datetime.today()
    #selecting latest articles
    articles = Article.objects.order_by('-created_at')[:HOME_VIEW_LIMIT]
    #filtering based on current user and upcoming events
    user_events = EventAttendee.objects.filter(user=request.user, status='accepted').values_list('event', flat=True)
    events = Event.objects.filter(
        id__in=user_events,
        event_date__gte=today
    ).order_by('event_date', 'event_time')[:HOME_VIEW_LIMIT]
    serialized_articles = ArticleSerializer(articles, many=True).data
    serialized_events = EventGuestSerializer(events, many=True).data
    data = {
        'articles': serialized_articles,
        'events': serialized_events
    }

    return success_response(data)