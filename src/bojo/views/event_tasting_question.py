from django.conf import settings
from rest_framework import viewsets, mixins
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import api_view, permission_classes

from src.users.models import User
from ..models import TastingQuestion, EventTastingQuestion, Event
from ..serializers import HostTastingQuestionSerializer
from src.common.serializers import success_response, error_response
from rest_framework.status import HTTP_400_BAD_REQUEST, HTTP_403_FORBIDDEN, HTTP_404_NOT_FOUND, HTTP_401_UNAUTHORIZED

"""
    method: get_tasting_questions
    params: event_id
    descripition:  For a given event based on its type will return questions
"""
@api_view(['GET'])
@permission_classes([IsAuthenticated])
def get_tasting_questions(request, event_id):
    free_sections_array = []
    premium_sections_array = []
    group_by_value = {}

    event_type = Event.objects.get(id=event_id).event_type

    #getting distinct sections for grouping
    value_list = TastingQuestion.objects.values_list(
        'host_section', flat=True
    ).distinct()

    #getting the id which the host as previously selected for the event
    event_selected_tasting_questions = EventTastingQuestion.objects.filter(event_id=event_id).values_list(
        'tasting_question__id', flat=True
    )

    #looping over each section
    for value in value_list:
        #getting the question for a particular section
        values = TastingQuestion.objects.filter(host_section=value, event_types__contains=[event_type])

        #get a serialized data for the questions
        filtered_event_tasting_questions = HostTastingQuestionSerializer(values, many=True).data

        #looping over each question in the sections event tasting question
        for question in filtered_event_tasting_questions:
            #checking if the question is alreay selected
            if question['id'] in event_selected_tasting_questions:
                question['host_selected'] = True
            else:
                question['host_selected'] = False
            
        section_questions = {
            "label": value.upper(),
            "questions": filtered_event_tasting_questions
        }

        if value in settings.FREE_SECTIONS:
            free_sections_array.append(section_questions)
        else:
            premium_sections_array.append(section_questions)

    group_by_value['event_type'] = settings.EVENT_TYPE_LABELS[event_type]
    group_by_value['free_sections'] = free_sections_array
    group_by_value['premium_sections'] = premium_sections_array
    
    return success_response(group_by_value)

"""
    method: update_event_tasting_questions
    descripition:  updates the event's tasting questions
"""
@api_view(['POST'])
@permission_classes([IsAuthenticated])
def update_event_tasting_questions(request):

    data = request.data
    new_tasting_questions = None
    event_id = None

    for field in data:
        if field  == 'tasting_questions':
            new_tasting_questions = data[field]
        elif field == 'event_id':
            event_id = data[field]
    
    if new_tasting_questions == None or event_id == None:
        return error_response({ "message": "event_id and tasting_questions are required" }, status=HTTP_400_BAD_REQUEST)
    
    try:   
        event = Event.objects.get(id=event_id)
    except Exception:
        return error_response({ "message": "event not found" }, status=HTTP_404_NOT_FOUND)

    #valid whether the request is coming from a host
    if not event.event_host == request.user:
        return error_response({ 'message': "not authorized"}, status=HTTP_401_UNAUTHORIZED)

    #getting the existing event questions
    existing_event_tasting_questions = EventTastingQuestion.objects.filter(event=1).values_list(
        'tasting_question__id', flat=True
    )

    #looping over existing event questions
    for existing_event_question_id in existing_event_tasting_questions:
        if existing_event_question_id not in new_tasting_questions:
            try:
                tasting_question = EventTastingQuestion.objects.get(tasting_question=existing_event_question_id)
            except EventTastingQuestion.DoesNotExist:
                return error_response({ "message": "Can't find question in this event" }, status=HTTP_400_BAD_REQUEST)
            tasting_question.delete()
        else:
            new_tasting_questions.remove(existing_event_question_id)
    """
        now if there are any elements in this array, then these are newly included
        ones and hence we must create a new record for these questions
    """
    for new_tasting_question_id in new_tasting_questions:
        event_tasting_question = EventTastingQuestion(
            event_id = 1,
            tasting_question_id = new_tasting_question_id
        )
        event_tasting_question.save()
    
    return success_response({'message': 'event questions updated successfully'})