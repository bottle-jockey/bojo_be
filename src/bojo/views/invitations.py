import uuid

from django.db.models import Q
from django.conf import settings
from smtplib import SMTPException
from rest_framework.parsers import JSONParser
from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import api_view, permission_classes, parser_classes
from rest_framework.status import HTTP_400_BAD_REQUEST, HTTP_401_UNAUTHORIZED, HTTP_404_NOT_FOUND, HTTP_403_FORBIDDEN

from src.common.serializers import success_response, error_response
from ..models import Invitation, Event, EventAttendee
from ..helpers import is_user_a_co_host
from src.users.models import User
from src.notifications.services import notify, ACTIVITY_INVITE_FOR_EVENT
from src.common.error_codes import MAXIMUM_EVENT_ATTENDEES_REACHED_ERROR_CODE

"""
    method: is_attendee_slot_remaining
    params: event, status
    descripition:  checks if slot for attendee in event is
                   still available
"""
def is_attendee_slot_remaining(event, status):
    if status == settings.EVENT_ATTENDEE_STATUS['ACCEPTED']:
        #get the no of attendee slots remaining
        attendees_count = EventAttendee.objects.filter(
            Q(event=event) & Q(is_taster=True) & Q(status=settings.EVENT_ATTENDEE_STATUS['ACCEPTED'])
        ).count()

        #checking for the number of attendees in the event
        if attendees_count == event.expected_attendees_count:
            return False
    return True

"""
    method: invite_guests
    params: request, event_id
            guests_email_id - array of attendees to be invited
    descripition:  Sends invite to valid email ids
"""
@api_view(['POST'])
@permission_classes([IsAuthenticated,])
@parser_classes([JSONParser,])
def invite_guests(request, event_id):
    data = request.data
    email_ids = []
    is_taster = False
    is_co_host = False

    #checking whether we are going to invite tasters or co_hosts
    if 'guests_email_id' in data:
        is_taster = True
        role = settings.INVITED_USER_ROLES['TASTER'].lower()
        email_ids = data['guests_email_id']
    elif 'co_hosts_email_id' in data:
        is_co_host = True
        role = settings.INVITED_USER_ROLES['CO_HOST'].lower()
        email_ids = data['co_hosts_email_id']
    else:
        return error_response({ 'message': "guest email ids or co host email ids is required"}, status=HTTP_400_BAD_REQUEST)

    try:   
        event = Event.objects.get(id=event_id)
    except Exception:
        return error_response({ "message": "event not found" }, status=HTTP_404_NOT_FOUND)
    
    guest_invite = None

    #checking if the event host is same as the current user
    if not (event.event_host == request.user or is_user_a_co_host(event, request.user)):
        return error_response({ 'message': "not authorized"}, status=HTTP_401_UNAUTHORIZED)

    total_invites = len(email_ids)
    invites_processed = 0
    
    for email in email_ids:
        try:
            validate_email(email)
            
            #checking if the email is already an in-app user
            user = User.get_active_user(email)
            if user:
                #checking if an invite is already sent to the user
                event_attendee = EventAttendee.check_if_user_already_invited(event=event, user=user)

                #if not sent create a new entry in the event_attendee table
                if not event_attendee:
                    event_attendee = EventAttendee(
                        event=event,
                        user=user,
                        email=email,
                        is_taster=is_taster,
                        is_co_host=is_co_host
                    )

                elif event_attendee.status == settings.EVENT_ATTENDEE_STATUS['DECLINED']:
                    """
                        if the status is rejected, then we are updating the status and
                        again sending the invite email
                    """
                    event_attendee.status = settings.EVENT_ATTENDEE_STATUS['AWAITING']
                
                #if the status is accepted already, we are skipping that email
                elif event_attendee.status == settings.EVENT_ATTENDEE_STATUS['ACCEPTED']:
                    continue
                
                guest_invite = event_attendee
                
            else:
                #checking if an invite is already sent to the user
                invitation = Invitation.check_if_already_invited(event=event, email=email)

                if not invitation:
                    invitation = Invitation(
                        email=email,
                        token=uuid.uuid1().hex,
                        event=event,
                        expires_at = event.event_date,
                        role=role
                    )
                
                guest_invite = invitation
            try:
                #sending invite emails
                invitation_url = settings.APP_SITE_URL+'invite-user/'+event.slug
                notify(
                    ACTIVITY_INVITE_FOR_EVENT,
                    email_to=email,
                    context={'event_name': event.name, 'event_invitation_url': invitation_url},
                    fail_silently=False
                )
                guest_invite.status = settings.INVITATION_STATUS['AWAITING']
            except Exception:
                guest_invite.status = settings.INVITATION_STATUS['FAILED']
            
            guest_invite.save()
        except ValidationError:
            continue
        
        invites_processed = invites_processed + 1

    response = {
        'total_invites': total_invites,
        'invites_processed': invites_processed,
        'message': 'successfully processed'
    }
    
    return success_response(response)

"""
    method: update_invite
    params: request, event_id
    descripition:  Handles user accept invite flow
"""
@api_view(['PUT'])
@permission_classes([IsAuthenticated,])
def update_invite(request, event_id):
    token = request.query_params.get('token')
    attendee_id = request.query_params.get('attendee_id')
    status = request.query_params.get('status')
    response_data = {
        'message': 'response recorded',
        'status': ''
    }
    
    if not status:
        return error_response(
            { 'message': "A valid status is required, status can be accept or decline"}, 
            status=HTTP_400_BAD_REQUEST
        )
    
    if status not in settings.INVITE_RESPONSES.keys():
        return error_response(
            { 'message': "A valid status is required, status can be accept or decline"},
            status=HTTP_400_BAD_REQUEST
        )
    
    if not event_id:
        return error_response({ 'message': "event_id is required"}, status=HTTP_400_BAD_REQUEST)
    
    #get the event corresponding to the invite
    try:
        event = Event.objects.get(id=event_id)
    except:
        return error_response({ 'message': "event not found"}, status=HTTP_404_NOT_FOUND)
        
    if token:
        """
            if token is passsed, then this the request is coming
            in from a newly signed up user
        """
        #get the invite record using the token
        try:
            invitation = Invitation.objects.get(token=token)
        except Invitation.DoesNotExist:
            return error_response({ 'message': "invalid token"}, status=HTTP_401_UNAUTHORIZED)
        
        #checking for attendees count only for tasters
        if invitation.role.lower() == settings.INVITED_USER_ROLES['TASTER'].lower():
            if not is_attendee_slot_remaining(event, settings.INVITE_RESPONSES[status]):
                return error_response(
                    { 'message': "No attendees slot is remaining for this event"},
                    code=MAXIMUM_EVENT_ATTENDEES_REACHED_ERROR_CODE,
                    status=HTTP_400_BAD_REQUEST
                )
        
        #update invitation
        invitation.status = settings.INVITE_RESPONSES[status]
        invitation.token = None
        invitation.save()

        #create a record in event_attendee
        event_attendee = EventAttendee(
            event=event,
            user=request.user,
            email=request.user.email,
            status=settings.INVITE_RESPONSES[status],
            is_co_host = invitation.role.lower() == settings.INVITED_USER_ROLES['CO_HOST'].lower(),
            is_taster = invitation.role.lower() == settings.INVITED_USER_ROLES['TASTER'].lower()
            #TODO bringing bottle
        )

        event_attendee.save()
            
    elif attendee_id:
        """
            if an attendee id is passed, then this is the request coming from
            an in app user
        """
        #find the event_attendee based on the attendee_id and update the status
        try:
            event_attendee = EventAttendee.objects.get(id=attendee_id)
        except EventAttendee.DoesNotExist:
            return error_response({ 'message': "event attendee not found"}, status=HTTP_400_BAD_REQUEST)

        #checking if the user already responded with the same status
        if settings.INVITE_RESPONSES[status] == event_attendee.status:
            return error_response(
                { 'message': "You have already {} the invite".format(settings.INVITE_RESPONSES[status])}, 
                status=HTTP_400_BAD_REQUEST
            )
        
        #checking for attendees count only for tasters
        if event_attendee.is_taster:
            if not is_attendee_slot_remaining(event, settings.INVITE_RESPONSES[status]):
                return error_response(
                    { 'message': "No attendees slot is remaining for this event"},
                    code=MAXIMUM_EVENT_ATTENDEES_REACHED_ERROR_CODE,
                    status=HTTP_400_BAD_REQUEST
                )

            event_attendee.status = settings.INVITE_RESPONSES[status]
            event_attendee.save()
    else:
        """
            if nothing is passed, then the request is coming in from a user 
            with the event's share link
        """
        invitation = None
        
        #first check if that user is already invited
        try:
            invitation = Invitation.objects.filter(
                Q(event=event) & Q(email=request.user.email) & Q(status=settings.INVITATION_STATUS['AWAITING'])
            ).get()

            #update invitation status
            invitation.status = settings.INVITE_RESPONSES[status]
            invitation.token = None
            invitation.save()

        except Invitation.DoesNotExist:
            try:
                event_attendee = EventAttendee.objects.get(event=event, user=request.user)

                #checking if the user already responded with the same status
                if settings.INVITE_RESPONSES[status] == event_attendee.status:
                    return error_response(
                        { 'message': "You have already {} the invite".format(settings.INVITE_RESPONSES[status])}, 
                        status=HTTP_400_BAD_REQUEST
                    )
                
                #checking for attendees count only for tasters
                if event_attendee.is_taster:
                    if not is_attendee_slot_remaining(event, settings.INVITE_RESPONSES[status]):
                        return error_response(
                            { 'message': "No attendees slot is remaining for this event"},
                            code=MAXIMUM_EVENT_ATTENDEES_REACHED_ERROR_CODE,
                            status=HTTP_400_BAD_REQUEST
                        )
                
                #update event attendee
                event_attendee.status = settings.INVITE_RESPONSES[status]
                event_attendee.save()
                
                response_data['status'] = format(settings.INVITE_RESPONSES[status])
                return success_response(response_data)
            except EventAttendee.DoesNotExist:
                if not invitation:
                    return error_response(
                        { 'message': "Your are not invited or not an attendee at this event"}, 
                        status=HTTP_400_BAD_REQUEST
                    )
        except Exception:
            return error_response({ 'message': "Somethig went wrong"}, status=HTTP_400_BAD_REQUEST)
        
        #checking for attendees count only for tasters
        if invitation.role.lower() == settings.INVITED_USER_ROLES['TASTER'].lower():
            if not is_attendee_slot_remaining(event, settings.INVITE_RESPONSES[status]):
                return error_response(
                    { 'message': "No attendees slot is remaining for this event"},
                    code=MAXIMUM_EVENT_ATTENDEES_REACHED_ERROR_CODE,
                    status=HTTP_400_BAD_REQUEST
                )
        
        event_attendee = EventAttendee(
            event=event,
            user=request.user,
            email=request.user.email,
            status=settings.INVITE_RESPONSES[status],
            is_co_host = False,
            is_taster = True
        )

        event_attendee.save()

    response_data['status'] = format(settings.INVITE_RESPONSES[status])
    return success_response(response_data)