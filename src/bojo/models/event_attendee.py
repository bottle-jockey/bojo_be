from django.db import models

STATUS_CHOICES = (
    ('awaiting', 'AWAITING'),
    ('accepted', 'ACCEPTED'),
    ('declined', 'DECLINED'),
    ('removed', 'REMOVED'),
    ('failed', 'FAILED'),
)

class EventAttendee(models.Model):
    """
        method: check_if_user_already_invited
        params: event, user
        descripition:  Check whether the user is already invited
    """
    @staticmethod
    def check_if_user_already_invited(event, user):
        try:
            event_attendee = EventAttendee.objects.get(event=event, user=user)
            if event_attendee:
                return event_attendee
        except EventAttendee.DoesNotExist:
            return None
        
    event = models.ForeignKey('bojo.Event', related_name='in_app_guests', on_delete=models.CASCADE)
    user = models.ForeignKey('users.User', related_name='user', on_delete=models.CASCADE)
    email = models.EmailField(('email address'))
    status = models.CharField(
        max_length=8,
        choices=STATUS_CHOICES,
        default='awaiting',
        null=False,
        blank=False,
    )
    is_co_host = models.BooleanField(default=False, blank=False, null=False)
    is_taster = models.BooleanField(default=True, blank=False, null=False)
    is_bringing_bottle = models.BooleanField(default=False, blank=False, null=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)