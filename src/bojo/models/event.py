from django.db import models
from django.conf import settings
from django.core.validators import MaxValueValidator, MinValueValidator
from src.users.models import User
from django.contrib.postgres.fields import ArrayField

EVENT_TYPE_CHOICES = (
    ('red_wine', 'RED_WINE'),
    ('white_wine', 'WHITE_WINE'),
)

EVENT_STATUS_CHOICES = (
    ('scheduled', 'SCHEDULED'),
    ('prepping', 'PREPPING'),
    ('started', 'STARTED'),
    ('ended', 'ENDED'),
)

class Event(models.Model):
    expected_attendees_count = models.IntegerField(
        blank=False, 
        null=False,
        validators=[
            MaxValueValidator(settings.MAXIMUM_ATTENDEES_COUNT),
            MinValueValidator(settings.MINIMUM_ATTENDEES_COUNT)
        ]
    )
    event_type = models.CharField(
        blank=False,
        null=False,
        max_length=10,
        choices=EVENT_TYPE_CHOICES,
    )
    is_attendees_bottles_required = models.BooleanField(default=True, blank=False, null=False)
    expected_bottles_count = models.IntegerField(
        blank=False, 
        null=False,
        default=1,
        validators=[
            MaxValueValidator(settings.MAXIMUM_BOTTLES_COUNT),
            MinValueValidator(settings.MINIMUM_BOTTLES_COUNT)
        ]
    )
    name = models.CharField(max_length=64, blank=False, null=False)
    contact_email = models.EmailField(('email address'), null=True, blank=True)
    contact_phone_number = models.CharField(null=True, blank=True, max_length=10)
    event_date = models.DateField(blank=False, null=False)
    event_time = models.TimeField(blank=False, null=False)
    address = models.JSONField(blank=True, null=True)
    coordinates = ArrayField(models.FloatField(), size=2, blank=True, null=True)
    details = models.TextField(blank=False, null=False)
    slug = models.SlugField(blank=False, null=False, unique=True)
    is_host_attending = models.BooleanField(default=True, blank=False, null=False)
    event_host = models.ForeignKey(User, on_delete=models.CASCADE)
    status = models.CharField(
        blank=False,
        null=False,
        max_length=9,
        choices=EVENT_STATUS_CHOICES,
    )
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)