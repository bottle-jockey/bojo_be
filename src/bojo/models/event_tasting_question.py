from django.db import models

class EventTastingQuestion(models.Model):
    event = models.ForeignKey('bojo.Event', on_delete=models.CASCADE)
    tasting_question = models.ForeignKey('bojo.TastingQuestion', on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
