from django.db import models

ROLE_TYPES = (
    ('taster', 'TASTER'),
    ('co_host', 'CO_HOST'),
)

STATUS_CHOICES = (
    ('awaiting', 'AWAITING'),
    ('failed', 'FAILED'),
    ('accepted', 'ACCEPTED'),
    ('declined', 'DECLINED'),
    ('lapsed', 'LAPSED'),
)

class Invitation(models.Model):
    """
        method: check_if_already_invited
        params: event, email
        descripition: Checks whether an event is laready sent to that
        email for the particular event
    """
    @staticmethod
    def check_if_already_invited(event, email):
        try:
            invitation = Invitation.objects.get(event=event, email=email)
            if invitation:
                return invitation
        except Invitation.DoesNotExist:
            return None
            
    email = models.EmailField(('email address'), null=False, blank=False)
    token = models.CharField(max_length=64, null=True, blank=True)
    status = models.CharField(
        max_length=8,
        choices=STATUS_CHOICES,
        default='awaiting',
        null=False,
        blank=False,
    )
    event = models.ForeignKey('bojo.Event', related_name='new_guests', on_delete=models.CASCADE)
    expires_at = models.DateField(null=False, blank=False)
    role = models.CharField(
        max_length=7,
        choices=ROLE_TYPES,
    )
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)