from django.db import models
from django.contrib.postgres.fields import ArrayField

EVENT_TYPE_CHOICES = (
    ('red_wine', 'RED_WINE'),
    ('white_wine', 'WHITE_WINE'),
)

PLAN_TYPE_CHOICES = (
    ('free', 'FREE'),
    ('premium', 'PREMIUM'),
)

QUESTION_TYPE_CHOICES = (
    ('text_area', 'TEXT_AREA'),
    ('star_rating', 'STAR_RATING'),
    ('text', 'TEXT'),
    ('radio_button', 'RADIO_BUTTON'),
    ('grouped_radio_button', 'GROUPED_RADIO_BUTTON'),
    ('checkbox_button', 'CHECKBOX_BUTTON'),
    ('grouped_boolean', 'GROUPED_BOOLEAN'),
    ('grouped_radio_button', 'GROUPED_RADIO_BUTTON'),
    ('slider_rating', 'SLIDER_RATING'),
)


class TastingQuestion(models.Model):
    event_types = ArrayField(
        models.CharField(
            blank=False,
            null=False,
            max_length=10,
            choices=EVENT_TYPE_CHOICES
        ), 
        size=2, 
        blank=False, 
        null=False
    )
    question_type = models.CharField(
        max_length=32,
        blank=False,
        null=False,
        choices=QUESTION_TYPE_CHOICES
    )
    label = models.CharField(max_length=32, blank=True, null=True)
    input_options = models.JSONField(blank=True, null=True)
    default_value = models.CharField(max_length=32, blank=True, null=True)
    order = models.IntegerField(blank=False, null=False)
    supported_plans = ArrayField(
        models.CharField(
            blank=False,
            null=False,
            max_length=7,
            choices=PLAN_TYPE_CHOICES,
        ), 
        size=2,
        blank=False,
        null=False
    )
    hint_text = models.CharField(max_length=32, blank=True, null=True)
    host_label = models.TextField(blank=False, null=False)
    host_description = models.TextField(blank=True, null=True)
    host_section = models.CharField(max_length=16, blank=False, null=False)
    is_default = models.BooleanField(default=False, blank=False, null=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)