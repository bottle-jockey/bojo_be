from django.db import models

# Create your models here.

TYPE_CHOICES = (
    ('text_only', 'TEXT_ONLY'),
    ('text_with_cover_image', 'TEXT_WITH_COVER_IMAGE'),
    ('text_with_video', 'TEXT_WITH_VIDEO'),
)

class Article(models.Model):
    title = models.CharField(max_length=128, blank=False, null=False)
    type = models.CharField(
        max_length=21,
        choices=TYPE_CHOICES,
        default='text_only',
    )
    description = models.TextField(blank=False, null=False)
    cover_image = models.ImageField(blank=True, null=True)
    video_embed = models.TextField(blank=True, null=True)
    url = models.CharField(max_length=128, blank=False, null=False)
    is_archieved = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)