from rest_framework.routers import SimpleRouter
from django.urls import path

from .views import (
    ArticleViewSet, EventViewSet, patch_event_attendee, home_view, invite_guests, 
    designate_co_host, remove_attendee_from_event, update_invite, get_tasting_questions,
    update_event_tasting_questions
)

bojo_router = SimpleRouter()

bojo_router.register(r'bojo/articles', ArticleViewSet)
bojo_router.register(r'bojo/events', EventViewSet,  basename='events')

urlpatterns = [
    path('', home_view),
    path('events/invite-guests/<int:event_id>', invite_guests),
    path('events/<int:event_id>/invite', update_invite),
    path('events/<int:event_id>/designate-co-host/<int:attendee_id>', designate_co_host),
    path('events/<int:event_id>/attendee/<int:attendee_id>', remove_attendee_from_event),
    path('events/<int:event_id>/host/tasting-questions', get_tasting_questions),
    path('event-attendees/<int:attendee_id>', patch_event_attendee),
    path('event/host/tasting-question', update_event_tasting_questions)
    
]