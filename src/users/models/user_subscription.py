from django.db import models

STATUS_TYPE_CHOICES = (
    ('active', 'ACTIVE'),
    ('expired', 'EXPIRED'),
)

PLAN_TYPE_CHOICES = (
    ('free', 'FREE'),
    ('premium', 'PREMIUM'),
)

class UserSubscription(models.Model):
    status = models.CharField(
        blank=False,
        null=False,
        max_length=7,
        choices=STATUS_TYPE_CHOICES,
    )
    plan = models.CharField(
        blank=False,
        null=False,
        max_length=7,
        choices=PLAN_TYPE_CHOICES,
    )
    expires_at = models.DateField(null=True)
    is_trial = models.BooleanField(blank=False, null=False, default=False)
    amount = models.DecimalField(
        max_digits=5,
        decimal_places=2,
        blank=False,
        null=False)
    currency = models.CharField(
        blank=False,
        null=False,
        max_length=3,
        default="USD"
    )
    paid_at = models.DateTimeField()
    user = models.ForeignKey('users.User', on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
