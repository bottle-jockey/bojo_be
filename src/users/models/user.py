import uuid
from datetime import datetime
from django.db import models
from django.conf import settings
from django.dispatch import receiver
from django.contrib.auth.models import BaseUserManager, AbstractUser
from rest_framework_simplejwt.tokens import RefreshToken
from easy_thumbnails.fields import ThumbnailerImageField
from django.urls import reverse
from django_rest_passwordreset.signals import reset_password_token_created
from easy_thumbnails.signals import saved_file
from easy_thumbnails.signal_handlers import generate_aliases_global

from src.common.helpers import build_absolute_uri
from src.notifications.services import notify, ACTIVITY_USER_RESETS_PASS
from .user_subscription import UserSubscription
from src.common.models import Meta

GENDER_CHOICES = (
    ('male', 'MALE'),
    ('female', 'FEMALE'),
    ('others', 'OTHERS'),
    ('prefer_not_to_say', 'PREFER_NOT_TO_SAY'),
)

STATUS_CHOICES = (
    ('new', 'NEW'),
    ('active', 'ACTIVE'),
    ('suspended', 'SUSPENDED'),
    ('deleted', 'DELETED'),
)

@receiver(reset_password_token_created)
def password_reset_token_created(sender, instance, reset_password_token, *args, **kwargs):
    """
    Handles password reset tokens
    When a token is created, an e-mail needs to be sent to the user
    """
    reset_password_path = reverse('password_reset:reset-password-confirm')
    context = {
        'username': reset_password_token.user.username,
        'email': reset_password_token.user.email,
        'reset_password_url': build_absolute_uri(f'{reset_password_path}?token={reset_password_token.key}'),
    }

    notify(ACTIVITY_USER_RESETS_PASS, context=context, email_to=[reset_password_token.user.email])

class UserManager(BaseUserManager):

    def create_user(self, email, password=None):
        if not email:
            raise ValueError('user must have an email address')
        user = self.model(email=email)
        #password won't be available for social auths hence checking for password
        if password:
            user.set_password(password)
        else:
            #for social auths updating user status to active
            user.status = 'active'
        user.save(using=self._db)
        subscription = user.add_subscription()
        return user

    def create_superuser(self, email, password):
        """
        Creates and saves a superuser with the given email and password.
        """
        user = self.create_user(
            email,
            password=password,
        )
        user.is_staff = True
        user.is_admin = True
        user.is_superuser = True
        user.save(using=self._db)
        return user

class User(AbstractUser):
    @staticmethod
    def get_active_user(email):
        try:
        #TODO update status
            user = User.objects.get(email=email, status='active')
        except User.DoesNotExist:
            return None
        return user
    """
        method: generate_member_id
        descripition: geneates the member id for a premium user
    """
    @staticmethod
    def generate_member_id():
        meta_member_id = Meta.objects.get(key="MEMBER_ID")

        member_id = int(meta_member_id.value) + 1
        is_id_genrated= False

        """
            Looping through to get the unique member_id
        """
        while(not is_id_genrated):
            try:
                user_count = User.objects.get(member_id=member_id)
                if user_count:
                    member_id = member_id + 1
                    continue
            except User.DoesNotExist:
                meta_member_id.value = str(member_id)
                meta_member_id.save()
                is_id_genrated = True
                break
            except Exception as e:
                break
        
        if is_id_genrated:
            return member_id
        return None

    username = None
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    email = models.EmailField(('email_address'), unique=True)
    phone_number = models.CharField(null=True, max_length=13, unique=True)
    phone_number_verified = models.BooleanField(default=False)
    otp = models.CharField(blank=True, null=True, max_length=6)
    otp_expires_at = models.DateTimeField(blank=True, null=True)
    otp_attempts = models.IntegerField(default=3)
    last_logged_in_at = models.DateTimeField(blank=True, null=True)
    member_id = models.IntegerField(blank=True, null=True)
    gender = models.CharField(
        max_length=17,
        choices=GENDER_CHOICES,
        default='prefer_not_to_say',
    )
    date_of_birth = models.DateField(blank=True, null=True)
    status = models.CharField(
        max_length=10,
        choices=STATUS_CHOICES,
        default='new',
    )
    profile_picture = ThumbnailerImageField('ProfilePicture', upload_to='profile_pictures/', blank=True, null=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    def get_tokens(self):
        refresh = RefreshToken.for_user(self)

        return {
            'refresh': str(refresh),
            'access': str(refresh.access_token),
        }
    """
        method: add_subscription
        params: plan type
        descripition: adds subscription to user
    """
    def add_subscription(self, plan=settings.SUBSCRIPTION_PLANS['FREE']):
        """
            If the type is premium then the previous free subscription is marked as expired
            and the new subscription of type premium is created for the user
        """
        subscription_amount = settings.FREE_SUBSCRIPTION_AMOUNT
        if plan == settings.SUBSCRIPTION_PLANS['PREMIUM']:
            current_subscription = UserSubscription.objects.get(
                user=self,
                status=settings.USER_STATUS['ACTIVE'],
                plan=settings.SUBSCRIPTION_PLANS['FREE']
            )
            current_subscription.status=settings.SUBSCRIPTION_STATUS['EXPIRED']
            current_subscription.save()
            member_id = User.generate_member_id()
            if member_id:
                self.member_id = member_id
                subscription_amount = settings.PREMIUM_SUBSCRIPTION_AMOUNT

        subscription = UserSubscription(
            status=settings.USER_STATUS['ACTIVE'], 
            plan=plan, 
            amount=subscription_amount,
            user=self, 
            paid_at=datetime.today()
        )
        subscription.save()
        return subscription
    
    """
        method: is_premium_user
        descripition: checks is the user is a premium user
    """
    def is_premium_user(self):
        try:
            subscription = UserSubscription.objects.get(
                user=self,
                plan=settings.SUBSCRIPTION_PLANS['PREMIUM'],
                status=settings.SUBSCRIPTION_STATUS['ACTIVE']
            )
        except UserSubscription.DoesNotExist:
            return False
        return True
    
    """
        method: get_active_subscription
        descripition: get users active subscription
    """    
    def get_active_subscription(self):
        try:
            subscription = UserSubscription.objects.get(
                user=self,
                status=settings.SUBSCRIPTION_STATUS['ACTIVE']
            )
            return subscription.plan
        except:
            raise Exception


    objects = UserManager()


saved_file.connect(generate_aliases_global)
