from django.contrib import admin

class UserSubscriptionAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'plan', 'status', 'expires_at', 'amount', 'currency', 'paid_at')