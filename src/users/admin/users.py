from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import gettext_lazy as _

from src.users.models import User

class UserAdmin(UserAdmin):
    search_fields = ('email',)
    list_display = ('email', 'last_name', 'first_name', 'gender', 'phone_number')
    list_filter = ('status',)
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2')}
        ),
    )
    fieldsets = ((None, {
        'fields': ('email', 'password')
    }), (_('Personal info'), {
        'fields': (
            'first_name',
            'last_name',
            'gender',
            'phone_number',
        )
    }), (_('Profile image'), {
        'fields': ('profile_picture',)
    }), (_('User status'), {
        'fields': ('status',)
    }), (_('Membership details'), {
        'fields': ('member_id',)
    }), (_('Important dates'), {
        'fields': ('last_login', 'date_joined')
    }))

    ordering = ['email',]
