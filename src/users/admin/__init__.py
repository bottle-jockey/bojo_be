from django.contrib import admin

from ..models import User
from ..models import UserSubscription
from .users import UserAdmin
from .user_subscription import UserSubscriptionAdmin

admin.site.register(User, UserAdmin)
admin.site.register(UserSubscription, UserSubscriptionAdmin)
