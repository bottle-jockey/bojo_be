from rest_framework import serializers

from src.users.models import User
from src.common.serializers import ThumbnailerJSONSerializer

class UserSerializer(serializers.ModelSerializer):
    # TODO unncomment this to generate thumbnails
    # profile_picture = ThumbnailerJSONSerializer(required=False,
    #                                             allow_null=True,
    #                                             alias_target='src.users')

    full_name = serializers.SerializerMethodField()

    def get_full_name(self, obj):
        return '{} {}'.format(obj.first_name, obj.last_name)

    class Meta:
        model = User
        fields = (
            'id',
            'email',
            'full_name',
            'first_name',
            'last_name',
            'gender',
            'date_of_birth',
            'phone_number',
            'phone_number_verified',
            'status',
            'profile_picture',
        )
        #read_only_fields = ('email', )


class CreateUserSerializer(serializers.ModelSerializer):
    profile_picture = ThumbnailerJSONSerializer(required=False, allow_null=True, alias_target='src.users')
    tokens = serializers.SerializerMethodField()

    def get_tokens(self, user):
        return user.get_tokens()

    def create(self, validated_data):
        # call create_user on user object. Without this
        # the password will be stored in plain text.
        user = User(email = validated_data['email'], first_name = validated_data['first_name'], last_name = validated_data['last_name'])
        user.set_password(validated_data['password'])
        user.save()
        return user

    class Meta:
        model = User
        fields = (
            'id',
            'email',
            'password',
            'first_name',
            'last_name',
            'tokens',
            'profile_picture',
        )
        read_only_fields = ('tokens', )
        extra_kwargs = {'password': {'write_only': True}}
