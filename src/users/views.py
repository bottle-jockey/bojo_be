from rest_framework import viewsets, mixins
from rest_framework.permissions import AllowAny, IsAuthenticated, IsAuthenticatedOrReadOnly
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework import status

from src.users.models import User
from src.common.permissions import IsUserOrReadOnly
from src.users.serializers import CreateUserSerializer, UserSerializer

from src.common.serializers import success_response, error_response

class UserViewSet(mixins.RetrieveModelMixin, mixins.UpdateModelMixin,
                  mixins.CreateModelMixin, viewsets.GenericViewSet):
    """
    Creates, Updates and Retrieves - User Accounts
    """
    queryset = User.objects.all()
    serializers = {
        'default': UserSerializer,
        'create': CreateUserSerializer
    }
    permissions = {
        'default': (IsUserOrReadOnly, IsAuthenticatedOrReadOnly,),
        'create': (AllowAny,),
        'strict_auth': (IsAuthenticated,)
    }

    def get_serializer_class(self):
        return self.serializers.get(self.action, self.serializers['default'])

    def get_permissions(self):
        if self.action == 'get_user_data' or self.action == 'post_user_heartbeat':
            self.permission_classes = self.permissions.get(self.action, self.permissions['strict_auth'])
        else:
            self.permission_classes = self.permissions.get(self.action, self.permissions['default'])
        return super().get_permissions()

    @action(detail=False, methods=['get'], url_path='me', url_name='me')
    def get_user_data(self, request):
        try:
            serializerd_data = UserSerializer(instance=request.user).data
            serializerd_data['active_subscription_plan'] = request.user.get_active_subscription()
            return success_response(serializerd_data, status=status.HTTP_200_OK)
        except Exception as e:
            return error_response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=False, methods=['post'], url_path='heartbeat', url_name='heartbeat')
    def post_user_heartbeat(self, request):
        try:
            user_serializer = UserSerializer(request.user)
            return success_response(user_serializer.data, status=status.HTTP_200_OK)
        except Exception as e:
            return error_response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
