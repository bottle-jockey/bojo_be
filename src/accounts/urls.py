from django.urls import path, include
from src.accounts import views

urlpatterns = [
    path('login', views.login_view),
    path('signup', views.signup_send_otp_view),
    path('update-phone-number', views.update_phone_number_view),
    path('verify-phone-number', views.verify_phone_number_view),
    path('resend-otp', views.resend_otp_view),
    path('upgrade', views.upgrade_to_premium_view),
    path('', views.update_account_view)
]