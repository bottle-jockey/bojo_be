
import os
import math, random

def generate_otp() :
    if os.getenv('DJANGO_DEBUG'):
        return "1234"
  
    # Declare a digits variable  
    # which stores all digits 
    digits = "0123456789"
    OTP = ""
  
   # length of password can be chaged
   # by changing value in range
    for i in range(4) :
        OTP += digits[math.floor(random.random() * 10)]
  
    return OTP
  