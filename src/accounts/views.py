import pytz
import datetime

from django.conf import settings
from django.shortcuts import render
from django.db import IntegrityError
from rest_framework.response import Response
from rest_framework.parsers import JSONParser, MultiPartParser, FormParser
from django.http import HttpResponse, JsonResponse
from django.contrib.auth import get_user_model
from rest_framework import exceptions
from rest_framework.permissions import AllowAny
from rest_framework.decorators import api_view, permission_classes
from django.views.decorators.csrf import ensure_csrf_cookie, csrf_exempt
from src.users.serializers import UserSerializer, CreateUserSerializer
from rest_framework.status import HTTP_400_BAD_REQUEST, HTTP_404_NOT_FOUND, HTTP_500_INTERNAL_SERVER_ERROR
from rest_framework.permissions import IsAuthenticated

from src.common.serializers import success_response, error_response
from src.common.services.messaging import send_OTP
from src.accounts.utils import generate_otp
from src.users.models import User, UserSubscription
from src.common.error_codes import ALREADY_PREMIUM_USER_ERROR_CODE
utc=pytz.UTC

@api_view(['GET'])
def profile(request):
    user = request.user
    serialized_user = UserSerializer(user).data
    return success_response({'user': serialized_user })

"""
    method: login_view
    params: email, password
    descripition:  for a valid email and password responds with tokens
"""
@api_view(['POST'])
@permission_classes([AllowAny])
def login_view(request):
    parser_classes = (JSONParser,)
    User = get_user_model()
    email = request.data['email']
    password = request.data['password']
    if (email is None) or (password is None):
        return error_response({ "message": "email and password required" }, status=HTTP_400_BAD_REQUEST)

    user = User.objects.filter(email=email).first()
    if(user is None or not user.check_password(password)):
        return error_response({ "message": "invalid email or password" }, status=HTTP_400_BAD_REQUEST)

    serialized_user = UserSerializer(user).data

    data = {
        'user': serialized_user,
    }

    #getting user's active subscription
    try:
        subscription_plan = user.get_active_subscription()
    except Exception:
        return error_response({ "message": "Something went wrong" }, status=HTTP_500_INTERNAL_SERVER_ERROR)

    data['user']['active_subscription_plan'] = subscription_plan
    
    if user.status == 'active':
        tokens = user.get_tokens()
        data['tokens'] = tokens

    return success_response(data)

"""
    method: signup_send_otp_view
    params: email, first_name, last_name, phone_number
    descripition:  signs in the user and send otp for phone_number verification.
    The status of the newly created user would be new         
"""
@api_view(['POST'])
@permission_classes([AllowAny])
def signup_send_otp_view(request):
    parser_classes = (JSONParser,)
    data = request.data
    serializer = UserSerializer(data=data)
    if serializer.is_valid():
        #generating otp
        otp = generate_otp()
        otp_expires_at = datetime.datetime.now() + datetime.timedelta(minutes=5)

        #constructing user
        user = User(
            email = data['email'],
            first_name = data['first_name'],
            last_name = data['last_name'],
            phone_number = data['phone_number'],
            otp = otp,
            otp_expires_at = otp_expires_at
        )

        #encrypting and saving password
        user.set_password(data['password'])

        # sending otp
        try:
            res = send_OTP(user.phone_number, otp)
        except Exception as e:
            return error_response({'message': e.args}, status=HTTP_500_INTERNAL_SERVER_ERROR)

        try:
            user.save()
        except IntegrityError as e:
            return error_response({'message': e.args}, status=HTTP_400_BAD_REQUEST)
        
        #create user subscription
        user.add_subscription()

        serialized_user = UserSerializer(user).data

        data = {
            'user': serialized_user,
        }

        return success_response(data)
    return error_response(serializer.errors, status=HTTP_400_BAD_REQUEST)

"""
    method: update_phone_number_view
    params: email, phone_number
    descripition:  For a given email updates the phone number and sends otp
    This method will be predominantly used for phone number verification for
    social login.       
"""
@api_view(['POST'])
@permission_classes([AllowAny])
def update_phone_number_view(request):
    email = request.data['email']
    phone_number = request.data['phone_number']

    if (phone_number is None):
        return error_response({ "message": "phone number required" }, status=HTTP_400_BAD_REQUEST)
    elif (email is None): 
        return error_response({ "message": "valid email is required" }, status=HTTP_400_BAD_REQUEST)

    try:
        user = User.objects.get(email=email)
    except User.DoesNotExist:
         return error_response({ "message": "user not found" }, status=HTTP_400_BAD_REQUEST)

    user.phone_number = phone_number
    user.phone_number_verified = False
    user.otp = generate_otp()
    user.otp_attempts = 3
    user.otp_expires_at = datetime.datetime.now() + datetime.timedelta(minutes=5)

    try:
        user.save()
    except IntegrityError as e:
        return error_response({'message': 'phone number already exists'}, status=HTTP_400_BAD_REQUEST)
    #TODO send otp
    return success_response({ 'message': "otp sent" })

"""
    method: verify_phone_number_view
    params: phone_number, otp
    descripition:  for a given phone number and otp, this method verifies the phone_number
    based on the otp and the status of the user will be updated to active     
"""
@api_view(['POST'])
@permission_classes([AllowAny])
def verify_phone_number_view(request):
    parser_classes = (JSONParser,)
    phone_number = request.data['phone_number']
    otp = request.data['otp']

    if (otp is None):
        return error_response({ "message": "otp required" }, status=HTTP_400_BAD_REQUEST)
    elif (phone_number is None):
        return error_response({ "message": "phone number required" }, status=HTTP_400_BAD_REQUEST)

    try:
        user = User.objects.get(phone_number=phone_number)
    except User.DoesNotExist:
        return error_response({ "message": "user not found" }, status=HTTP_404_NOT_FOUND)

    #checking if phone number is already verified
    if user.phone_number_verified:
        return error_response({ "message": "phone number already verified" }, status=HTTP_400_BAD_REQUEST)

    #checking expiry
    otp_expiry = user.otp_expires_at
    if otp_expiry < utc.localize(datetime.datetime.now()):
        return error_response({ 'message': "otp expired" }, status=HTTP_400_BAD_REQUEST)

    #validating otp
    if user.otp == otp:
        user.otp_attempts = 3
        user.status = 'active'
        user.phone_number_verified = True
        user.save()
        tokens = user.get_tokens()

        data = {
            'message': "phone number verified",
            'tokens': tokens
        }

        return success_response(data)
    
    #if iinvalid otp decrementing otp_attempts
    user.otp_attempts = user.otp_attempts - 1
    user.save()
    return error_response({ 'message': "invalid otp" }, status=HTTP_400_BAD_REQUEST)

"""
    method: resend_otp_view
    params: phone_number
    descripition:  resends otp for a given phone_number
"""
@api_view(['POST'])
@permission_classes([AllowAny])
def resend_otp_view(request):
    phone_number = request.data['phone_number']

    if (phone_number is None):
        return error_response({ "message": "phone number required" }, status=HTTP_400_BAD_REQUEST)

    try:
        user = User.objects.get(phone_number=phone_number)
    except User.DoesNotExist:
        return error_response({ "message": "user not found" }, status=HTTP_404_NOT_FOUND)

    user.otp = generate_otp()
    user.otp_attempts = 3
    user.otp_expires_at = datetime.datetime.now() + datetime.timedelta(minutes=5)

    user.save()
    #sending otp
    try:
        res = send_OTP(user.phone_number, user.otp)
    except Exception as e:
        return error_response({'message': e.args}, status=HTTP_500_INTERNAL_SERVER_ERROR)
    return success_response({ 'message': "otp resent" })

"""
    method: update_account_view
    descripition:  updates the current user
"""
@api_view(['PUT'])
@permission_classes([IsAuthenticated])
def update_account_view(request):
    parser_classes = (MultiPartParser, FormParser)
    try:
        user = User.objects.get(email=request.user)
    except User.DoesNotExist:
        return HttpResponse(status=HTTP_404_NOT_FOUND)

    request.data._mutable = True
    data = request.data
    data['email'] = user.email

    serializer = UserSerializer(user, data=data)

    if serializer.is_valid():
        serializer.save()
        return success_response(serializer.data)
    return error_response(serializer.errors, status=HTTP_400_BAD_REQUEST)

"""
    method: upgrade_to_premium_view
    descripition: upgrades user as premium user
"""
@api_view(['POST'])
@permission_classes([IsAuthenticated])
def upgrade_to_premium_view(request):
    try:
        user = User.objects.get(email=request.user.email)
    except User.DoesNotExist:
        return error_response({'message': 'User not found'}, status=HTTP_404_NOT_FOUND)
    
    if user.is_premium_user():
        return error_response (
            {
                'message': 'Already a premium user'
            },
            code=ALREADY_PREMIUM_USER_ERROR_CODE, 
            status=HTTP_400_BAD_REQUEST
        )
    
    try:
        user.add_subscription(settings.SUBSCRIPTION_PLANS['PREMIUM'])
        user.save()
    except Exception:
        return error_response({'message': 'Something went wrong'}, status=HTTP_500_INTERNAL_SERVER_ERROR)

    response = {
        'member_id': user.member_id,
        'message': 'Account upgraded to premium successfully'
    }
    return success_response(response)